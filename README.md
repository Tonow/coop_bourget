# Coop_bourget

django + docker project : [doc](https://docs.docker.com/compose/django/)

## Lunch

* `sudo docker-compose up`

* To use pdb start like `sudo docker-compose run --service-ports web`

## Create superuser

* `sudo docker-compose run web python manage.py createsuperuser`

## Refrech model

* `sudo docker-compose run web python manage.py makemigrations`
* `sudo docker-compose run web python manage.py migrate`
