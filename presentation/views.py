from django.shortcuts import render, redirect
from .forms import CommentaireForm
from presentation.models import UserComment

def accueil(request):
    """ Form block.
    """
    envoi_ok = False
    # Construire le formulaire, soit avec les données postées,
    # soit vide si l'utilisateur accède pour la première fois
    # à la page.
    if request.method == 'POST':
        form_comment = CommentaireForm(request.POST)
    # Nous vérifions que les données envoyées sont valides
    # Cette méthode renvoie False s'il n'y a pas de données
    # dans le formulaire ou qu'il contient des erreurs.
        if form_comment.is_valid():
            form_comment.save()
            envoi_ok = True
    else:
        form_comment = CommentaireForm()

    return render(request, 'presentation/accueil.html', {
            'form_comment': form_comment,
            'envoi_ok': envoi_ok,
        })
