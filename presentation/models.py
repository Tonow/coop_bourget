from django.db import models
from django import forms
from django.utils import timezone


class UserComment(models.Model):
    date = models.DateTimeField(default=timezone.now, verbose_name="Date")
    interess = models.BooleanField(verbose_name="Etes vous interesser")
    invest = models.BooleanField(verbose_name="Soutaiteriez vous investire")
    mail = models.EmailField(verbose_name="Pour avoir des nouvelles veuillez nous laisser un mail", blank=True)
    text = models.TextField(verbose_name='Commentaire eventuel', blank=True)

    class Meta:
        verbose_name = "Commentaire"
        ordering = ['date']


    def __str__(self):
        """
        Cette méthode que nous définirons dans tous les modèles
        nous permettra de reconnaître facilement les différents objets que
        nous traiterons plus tard dans l'administration
        """
        return self.mail
