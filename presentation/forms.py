from django import forms
from django.core.exceptions import ValidationError
from .models import UserComment
from .insulte import liste_insulte

class CommentaireForm(forms.ModelForm):

    def clean_text(self):
        cleaned_data = super(CommentaireForm, self).clean()
        text = self.cleaned_data.get('text')
        words = text.lower().split()
        for terme in liste_insulte:
            if terme in words:
                raise forms.ValidationError("Désoler vous avez mis ce terme qui nous parais grossier {} veuillez reformuler".format(terme))

        return text  # Ne pas oublier de renvoyer le contenu du champ traité

    def clean_mail(self):
        cleaned_data = super(CommentaireForm, self).clean()
        mail = self.cleaned_data['mail']
        if mail == '':
            return mail
        if UserComment.objects.filter(mail=mail).exists():
            raise ValidationError("Désoler cette adresse '{}' a déja été utiliser".format(mail))
        return mail


    class Meta:
        model = UserComment
        # fields = '__all__'
        # fields = ('one_field',)
        # exclude = ['date']
        fields = ('interess', 'invest', 'mail', 'text')
        # widgets = {
        #     'mail': Textarea('placeholder': 'Coucou'}),
        # }
