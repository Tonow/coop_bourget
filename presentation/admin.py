from django.contrib import admin
from django.utils.text import Truncator
from .models import UserComment
# Register your models here.


class CommentAdmin(admin.ModelAdmin):
   list_display   = ('lead', 'interess', 'invest', 'mail', 'apercu_commentaire', 'date')
   # filter_horizontal = ('interess',)
   list_filter    = ('interess','invest', 'mail')
   date_hierarchy = 'date'
   ordering       = ('date',)
   search_fields  = ('mail', 'text')

   def apercu_commentaire(self, Commentaire):
      """
      Retourne les 40 premiers caractères du commentaire,
      suivi de points de suspension si le texte est plus long.
      """
      return Truncator(Commentaire.text).chars(40, truncate='...')

   # En-tête de notre colonne
   apercu_commentaire.short_description = 'Aperçu du commentaire'


   def lead(self, Commentaire):
       """Returns the potential lead.
       """
       lead = 0
       if Commentaire.interess:
           lead += 1
       if Commentaire.invest:
           lead += 10
       if Commentaire.mail:
           lead += 100
       return lead


admin.site.register(UserComment, CommentAdmin)
